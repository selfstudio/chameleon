//
//  ViewController.swift
//  ChameleonDemo
//
//  Created by lincolnlaw on 2017/6/25.
//  Copyright © 2017年 lincolnlaw. All rights reserved.
//

import UIKit
import Chameleon
extension Chameleon {
    static func customSkin() {
        Chameleon.Skin.addComponents(from: [SkinManager.self])
    }
}

struct SkinManager: ChameleonSkinComponentCompetible {
    static func components() -> [Chameleon.Skin.Style: Set<Chameleon.Component>] {

        let white = UIColor(hex6: 0xF9F9F9)

        let gray = UIColor(hex6: 0x4B4B4B)
        //        let logoGreen = UIColor(hex6: 0x58DE70)
        let green = UIColor(hex6: 0x4CD964)
        let font = UIFont.systemFont(ofSize: 12, weight: .light)

        var dayInfo: Set<Chameleon.Component> = []
        let pic = #imageLiteral(resourceName: "pic.jpg").withRenderingMode(.alwaysOriginal)
        let pic2 = #imageLiteral(resourceName: "pic2.jpg").withRenderingMode(.alwaysOriginal)
        dayInfo.insert(.custom(.tabbarImage(pic), 1))
        dayInfo.insert(.custom(.tabbarSelectedImage(pic2), 1))
        dayInfo.insert(.custom(.tabbarImage(pic), 2))
        dayInfo.insert(.custom(.tabbarSelectedImage(pic2), 2))
        dayInfo.insert(.custom(.imageForState(pic, .normal), 10))
        dayInfo.insert(.preferredStatusBarStyle(UIStatusBarStyle.default))
        dayInfo.insert(.backgroundColor(white))
        dayInfo.insert(.navigationbarBarTintColor(white))
        dayInfo.insert(.navigationbarTintColor(gray))
        dayInfo.insert(.tabbarTintColor(green))
        dayInfo.insert(.tabbarBarTintColor(white))
        dayInfo.insert(.custom(.borderWidth(5), 11))
        dayInfo.insert(.custom(.borderColor(UIColor.green), 11))
        dayInfo.insert(.custom(.shadowColor(UIColor.green), 11))
        dayInfo.insert(.titleTextAttributesForState([.foregroundColor: UIColor(hex6: 0x6C7682), .font: font], .normal))
        dayInfo.insert(.titleTextAttributesForState([.foregroundColor: green, .font: font], .selected))

        let gray2 = UIColor(hex6: 0x262626)
        var nightInfo: Set<Chameleon.Component> = []
        nightInfo.insert(.custom(.tabbarImage(pic2), 1))
        nightInfo.insert(.custom(.tabbarSelectedImage(pic), 1))
        nightInfo.insert(.custom(.tabbarImage(pic2), 2))
        nightInfo.insert(.custom(.tabbarSelectedImage(pic), 2))
        nightInfo.insert(.custom(.imageForState(pic2, .normal), 10))
        nightInfo.insert(.preferredStatusBarStyle(UIStatusBarStyle.lightContent))
        nightInfo.insert(.backgroundColor(UIColor(hex6: 0x1B1B1B)))
        nightInfo.insert(.navigationbarBarTintColor(gray2))
        nightInfo.insert(.navigationbarTintColor(white))
        nightInfo.insert(.tabbarTintColor(green))
        nightInfo.insert(.tabbarBarTintColor(gray2))
        nightInfo.insert(.custom(.borderWidth(10), 11))
        nightInfo.insert(.custom(.borderColor(UIColor.yellow), 11))
        nightInfo.insert(.custom(.shadowColor(UIColor.yellow), 11))
        nightInfo.insert(.titleTextAttributesForState([.foregroundColor: UIColor(hex6: 0x6C7682), .font: font], .normal))
        nightInfo.insert(.titleTextAttributesForState([.foregroundColor: green, .font: font], .selected))
        return [Chameleon.Skin.Style.day: dayInfo, Chameleon.Skin.Style.night: nightInfo]
    }
}

final class Navi: UINavigationController {
    private var _style: UIStatusBarStyle = .default
    override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
    }

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    convenience init() {
        self.init(rootViewController: TabController())
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        Chameleon.toDay()
        view.chameleon.register(.backgroundColor).apply()
        navigationBar.isTranslucent = false
        navigationBar.chameleon.registerSet([.navigationbarTintColor, .navigationbarBarTintColor]).apply()
        Chameleon.customSkin()
        Chameleon.Skin.day.save()
        chameleon.skinDidChanged = { [unowned self] _, skin in
            if let value = skin.component(for: .preferredStatusBarStyle), case let Chameleon.Component.preferredStatusBarStyle(style) = value {
                self._style = style
                DispatchQueue.main.async { self.setNeedsStatusBarAppearanceUpdate() }
            }
        }
    }

    override var preferredStatusBarStyle: UIStatusBarStyle { return _style }
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation { return .fade }
}

final class TabController: UITabBarController {
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    convenience init() {
        self.init(nibName: nil, bundle: nil)
        view.chameleon.register(.backgroundColor)

        let news = ViewController()
        news.title = "News"
        news.navigationItem.title = "News"
        let vc2 = ViewController2()
        vc2.title = "Haha"
        setViewControllers([news, vc2], animated: false)
        viewControllers?.forEach({ $0.view.chameleon.register(.backgroundColor) })
        tabBar.chameleon.registerSet([.tabbarTintColor, .tabbarBarTintColor])
        tabBar.isTranslucent = false
        guard let items = tabBar.items else { return }
        for (index, item) in items.enumerated() {
            item.tag = index + 1
            item.chameleon.registerSet([.custom(.tabbarImage, item.tag), .titleTextAttributesForStateNormal, .titleTextAttributesForStateSelected, .custom(.tabbarSelectedImage, item.tag)])
        }
    }
}

class ViewController: UIViewController {

    private var _count = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "xixi"
        view.chameleon.register(.backgroundColor)
        navigationController?.navigationBar.chameleon.registerSet([.barStyle, .navigationbarTintColor, .titleTextAttributes, .tintColor])
    }

    override func touchesEnded(_: Set<UITouch>, with _: UIEvent?) {
        _count += 1
        _count % 2 == 0 ? Chameleon.toDay() : Chameleon.toNight()
    }
}

class ViewController2: UIViewController {

    private var _count = 0
    private var _button = UIButton()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "hahha"
        view.chameleon.register(.backgroundColor).apply()
        view.addSubview(_button)
        _button.tag = 10
        _button.layer.tag = 11
        _button.frame = CGRect(x: 100, y: 100, width: 100, height: 80)
        _button.layer.shadowOffset = CGSize(width: 1, height: 1)
        _button.layer.shadowPath = UIBezierPath(rect: _button.bounds).cgPath
        _button.layer.shadowOpacity = 0.5
        _button.layer.chameleon.registerSet([.custom(.borderColor, 11), .custom(.borderWidth, 11), .custom(.shadowColor, 11)])
        _button.chameleon.registerSet([.tintColor, .titleColorForStateNormal, .backgroundImageForStateNormal, .custom(.imageForStateNormal, _button.tag)])
    }

    override func touchesEnded(_: Set<UITouch>, with _: UIEvent?) {
        _count += 1
        _count % 2 == 0 ? Chameleon.toDay() : Chameleon.toNight()
    }
}

class ViewController3: UIViewController {

    @IBOutlet weak var _tableview: UITableView!

    private var _count = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "hoho"
        view.chameleon.register(.backgroundColor)
    }
}

extension ViewController3: UITableViewDataSource, UITableViewDelegate {
    func numberOfSections(in _: UITableView) -> Int {
        return 2
    }

    func tableView(_: UITableView, numberOfRowsInSection _: Int) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "ThemeCell", for: indexPath) as! ThemeCell
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "NormalCell", for: indexPath) as! NormalCell
            return cell
        }
    }

    func tableView(_: UITableView, heightForRowAt _: IndexPath) -> CGFloat {
        return 204
    }

    func tableView(_: UITableView, didSelectRowAt _: IndexPath) {
        _count += 1
    }
}

final class ThemeCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel! {
        didSet {
            titleLabel?.chameleon.register(.title).apply()
        }
    }

    @IBOutlet weak var subTitleLabel: UILabel! {
        didSet {
            subTitleLabel?.chameleon.register(.subTitle).apply()
        }
    }

    @IBOutlet weak var bodyLabel: UILabel! {
        didSet {
            bodyLabel?.chameleon.register(.body).apply()
        }
    }

    @IBOutlet weak var footer: UILabel! {
        didSet {
            footer?.chameleon.register(.footer).apply()
        }
    }

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func awakeFromNib() {
        contentView.chameleon.register(.backgroundColor).apply()
        selectionStyle = .none
    }
}

final class NormalCell: UITableViewCell {
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func awakeFromNib() {
        selectionStyle = .none
    }
}
