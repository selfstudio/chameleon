//
//  ChameleonProtocols.swift
//  Chameleon
//
//  Created by lincolnlaw on 2017/7/7.
//  Copyright © 2017年 lincolnlaw. All rights reserved.
//

import Foundation
// MARK: - ChameleonCompetible

public typealias ChameleonWizard = ChameleonCompetible & ChameleonAnimateCompetible & ChameleonTagProvidable
public protocol ChameleonCompetible {
    associatedtype Base
    var chameleon: Base { get }
}

extension ChameleonAnimateCompetible {
    public var chameleonInfo: [Chameleon.Component.Key: Selector] { return [:] }
}

private var kChameleonWatcher: Void?
public extension ChameleonCompetible where Self: NSObjectProtocol & ChameleonAnimateCompetible & ChameleonTagProvidable {
    public var chameleon: ChameleonWatcher<Self> {
        if let pre = objc_getAssociatedObject(self, &kChameleonWatcher) as? ChameleonWatcher<Self> { return pre }
        let now: ChameleonWatcher<Self> = ChameleonWatcher(self)
        objc_setAssociatedObject(self, &kChameleonWatcher, now, .OBJC_ASSOCIATION_RETAIN_NONATOMIC)
        return now
    }
}

// MARK: - ChameleonAnimateCompetible
public protocol ChameleonAnimateCompetible {
    var chameleonInfo: [Chameleon.Component.Key: Selector] { get }
}

// MARK: - ChameleonTagProvidable
public protocol ChameleonTagProvidable { var tag: Int { get } }
extension ChameleonTagProvidable {

    func isMyTag(_ value: String?) -> Bool {
        guard let v = value else { return true }
        return v.toInt == tag
    }

    func isMyTagForState(_ value: String?) -> (UIControl.State?, Bool) {
        guard let kv = value?.components(separatedBy: "|"), let rawState = kv.first, let uintState = UInt(rawState), let rawTag = kv.last, let _tag = Int(rawTag), _tag == tag else { return (nil, false) }
        return (UIControl.State(rawValue: uintState), true)
    }
}

// MARK: -
public protocol ChameleonSkinComponentCompetible {
    static func components() -> [Chameleon.Skin.Style: Set<Chameleon.Component>]
}
