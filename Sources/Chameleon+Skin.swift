//
//  Chameleon+Skin.swift
//  Chameleon
//
//  Created by Lincoln Law on 2019/1/9.
//  Copyright © 2019 lincolnlaw. All rights reserved.
//

import Foundation

// MARK: Model
extension Chameleon {
    /// Skin
    public final class Skin: Hashable, Codable {
        
        public var hashValue: Int { return name.hashValue }
        
        public let name: Style
        public private(set) var components: Set<Chameleon.Component>
        public init(name: Style, components: Set<Chameleon.Component>) {
            self.name = name
            self.components = components
        }
        
        // MARK: Codable
        public init(from decoder: Decoder) throws {
            var _components: Set<Chameleon.Component> = []
            let values = try decoder.container(keyedBy: CodingKeys.self)
            let rawStyle = try values.decodeIfPresent(String.self, forKey: .name) ?? ""
            name = Style.from(raw: rawStyle)
            let raw = try values.decodeIfPresent(String.self, forKey: .components)
            if let data = raw?.data(using: .utf8) {
                if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: String] {
                    _components = Chameleon.Component.instance(from: json, style: name)
                }
            }
            components = _components
        }
        
        public func encode(to encoder: Encoder) throws {
            var container = encoder.container(keyedBy: CodingKeys.self)
            try container.encode(name.name, forKey: .name)
            let json = Chameleon.Component.json(for: components, in: self)
            try container.encode(json, forKey: .components)
        }
        
        private enum CodingKeys: String, CodingKey {
            case name
            case components
        }
    }
}

// MARK: - Public API
extension Chameleon.Skin {
    
    public func component(for key: Chameleon.Component.Key) -> Chameleon.Component? {
        return components.filter { $0.key == key.value }.first
    }
    
    public func save() {
        DispatchQueue.global(qos: .userInitiated).async {
            let encoder = JSONEncoder()
            guard let data = try? encoder.encode(self) else { return }
            let path = Chameleon.jsonFile(for: self.name)
            let url = URL(fileURLWithPath: path)
            try? data.write(to: url)
        }
    }
    
    public func delete() {
        DispatchQueue.global(qos: .userInitiated).async {
            let path = Chameleon.jsonFile(for: self.name)
            try? FileManager.default.removeItem(atPath: path)
        }
    }
}
// MARK: - Static func
extension Chameleon.Skin {
    public static func from(name: Style) -> Chameleon.Skin? {
        let path = Chameleon.jsonFile(for: name)
        guard FileManager.default.fileExists(atPath: path) else { return nil }
        let url = URL(fileURLWithPath: path)
        do {
            let data = try Data(contentsOf: url)
            let decoder = JSONDecoder()
            let skin = try decoder.decode(Chameleon.Skin.self, from: data)
            return skin
        } catch { print("decode error: \(error)") }
        return nil
    }
    
    // MARK: Static
    public static func ==(lhs: Chameleon.Skin, rhs: Chameleon.Skin) -> Bool {
        return lhs.name == rhs.name
    }
    
    public static let none: Chameleon.Skin = Chameleon.Skin(name: .none, components: [])
    public private(set) static var day: Chameleon.Skin = Chameleon.Skin(name: .day, components: [])
    public private(set) static var night: Chameleon.Skin = Chameleon.Skin(name: .night, components: [])
    
    private func update(from data: Set<Chameleon.Component>) {
        components = components.union(data)
    }
    
    public static func addComponents(from vendor: [ChameleonSkinComponentCompetible.Type], notifyChange: Bool = true) {
        vendor.flatMap { $0.components() }.forEach { map in
            let (key, value) = map
            if key == .day {
                Chameleon.Skin.day.update(from: value)
            } else if key == .night {
                Chameleon.Skin.night.update(from: value)
            }
        }
        guard notifyChange else { return }
        NotificationCenter.default.post(name: .ChameleonSkinChanged, object: nil)
    }
}

extension Chameleon.Skin {
    public enum Style: Hashable {
        public var hashValue: Int { return name.hashValue }
        
        public static func ==(lhs: Chameleon.Skin.Style, rhs: Chameleon.Skin.Style) -> Bool {
            return lhs.name == rhs.name
        }
        
        public static func from(raw: String) -> Style {
            switch raw {
            case Style.none.name: return .none
            case Style.day.name: return .day
            case Style.night.name: return .night
            default: return Style.custom(raw)
            }
        }
        var name: String {
            switch self {
            case let .custom(name): return name
            default: return "\(self)"
            }
        }
        case none, day, night, custom(String)
    }
}
