//
//  ChameleonWatcher.swift
//  Chameleon
//
//  Created by lincolnlaw on 2017/7/7.
//  Copyright © 2017年 lincolnlaw. All rights reserved.
//

import Foundation
// MARK: - ChameleonWatcher
public final class ChameleonWatcher<Base: NSObjectProtocol & ChameleonAnimateCompetible & ChameleonTagProvidable> {
    private weak var base: Base?
    private var _registerPool: Set<Chameleon.Component.Key> = []
    private var _availableKeys: [Chameleon.Component.Key: Selector] = [:]
    private var _observer: NSObjectProtocol?

    public var skinDidChanged: (Base?, Chameleon.Skin) -> Void = { _, _ in }
    deinit { if let ob = _observer { NotificationCenter.default.removeObserver(ob) } }

    public init(_ base: Base) {
        self.base = base
        _availableKeys = base.chameleonInfo
        startObserver()
    }

    private func startObserver() {
        _observer = NotificationCenter.default.addObserver(forName: NSNotification.Name.ChameleonSkinChanged, object: nil, queue: nil) { [unowned self] _ in
            DispatchQueue.main.async {
                if let tag = self.base?.tag {
                    DispatchQueue.global(qos: .userInitiated).async {
                        self.handleSkinChanged(tag: tag)
                    }
                }
                self.skinDidChanged(self.base, Chameleon.skin)
            }
        }
    }

    private func handleSkinChanged(animated _: Bool = true, tag: Int) {
        guard let base = base else { return }
        let skin = Chameleon.skin
        var actions: [Chameleon.Action] = []

        for key in _registerPool {
            guard let sel = _availableKeys[key], let component = skin.component(for: key) else {
                for (_key, _) in _availableKeys {
                    if Chameleon.Component.Key.custom(_key, tag) == key, let component = skin.component(for: key), let sel = _availableKeys[_key] {
                        let (value1, value2) = valuesFrom(component: component)
                        actions.append {
                            base.perform(sel, with: value1, with: value2)
                        }
                        break
                    }
                }
                continue
            }
            let (value1, value2) = valuesFrom(component: component)
            actions.append {
                base.perform(sel, with: value1, with: value2)
            }
        }
        DispatchQueue.main.async {
            actions.forEach({ $0() })
        }
    }

    private func valuesFrom(component: Chameleon.Component) -> (value: Any?, value2: String?) {
        var value1: Any?
        var value2: String?
        switch component {
        case let .title(color): value1 = color
        case let .subTitle(color): value1 = color
        case let .body(color): value1 = color
        case let .footer(color): value1 = color
        case let .activityIndicatorViewStyle(style): value1 = "\(style.rawValue)"
        case let .alpha(alpha): value1 = "\(alpha)"
        case let .backgroundColor(color): value1 = color
        case let .backgroundImageForState(color, state): value1 = color; value2 = "\(state.rawValue)"
        case let .barStyle(style): value1 = "\(style.rawValue)"
        case let .navigationbarStyle(style): value1 = "\(style.rawValue)"
        case let .tabbarStyle(style): value1 = "\(style.rawValue)"
        case let .toolbarStyle(style): value1 = "\(style.rawValue)"
        case let .searchbarStyle(style): value1 = "\(style.rawValue)"
        case let .barTintColor(color): value1 = color
        case let .navigationbarTintColor(color): value1 = color
        case let .tabbarTintColor(color): value1 = color
        case let .toolbarTintColor(color): value1 = color
        case let .searchbarTintColor(color): value1 = color
        case let .borderColor(color): value1 = color
        case let .borderWidth(width): value1 = "\(width)"
        case let .currentPageIndicatorTintColor(color): value1 = color
        case let .font(font): value1 = font
        case let .labelFont(font): value1 = font
        case let .textFieldFont(font): value1 = font
        case let .textViewFont(font): value1 = font
        case let .highlightedTextColor(color): value1 = color
        case let .image(image): value1 = image
        case let .tabbarImage(image): value1 = image
        case let .tabbarSelectedImage(image): value1 = image
        case let .imageForState(image, state): value1 = image; value2 = "\(state.rawValue)"
        case let .keyboardAppearance(style): value1 = "\(style.rawValue)"
        case let .textFieldKeyboardAppearance(style): value1 = "\(style.rawValue)"
        case let .textViewKeyboardAppearance(style): value1 = "\(style.rawValue)"
        case let .maximumTrackTintColor(color): value1 = color
        case let .minimumTrackTintColor(color): value1 = color
        case let .onTintColor(color): value1 = color
        case let .pageIndicatorTintColor(color): value1 = color
        case let .preferredStatusBarStyle(style): value1 = "\(style.rawValue)"
        case let .progressTintColor(color): value1 = color
        case let .separatorColor(color): value1 = color
        case let .shadowColor(color): value1 = color
        case let .textColor(color): value1 = color
        case let .labelTextColor(color): value1 = color
        case let .textFieldTextColor(color): value1 = color
        case let .textViewTextColor(color): value1 = color
        case let .thumbTintColor(color): value1 = color
        case let .tintColor(color): value1 = color
        case let .titleColorForState(color, state): value1 = color; value2 = "\(state.rawValue)"
        case let .titleTextAttributes(json): value1 = json
        case let .titleTextAttributesForState(json, state): value1 = json; value2 = "\(state.rawValue)"
        case let .trackTintColor(color): value1 = color
        case let .navigationbarBarTintColor(color): value1 = color
        case let .tabbarBarTintColor(color): value1 = color
        case let .toolbarBarTintColor(color): value1 = color
        case let .searchbarBarTintColor(color): value1 = color
        case let .strokeColor(color): value1 = color
        case let .fillColor(color): value1 = color
        case let .custom(customComponent, tag):
            let (one, two) = valuesFrom(component: customComponent)
            value1 = one
            if let t = two { value2 = "\(t)|\(tag)" }
            else { value2 = "\(tag)" }
        }
        return (value1, value2)
    }

    /// register key
    ///
    /// - Parameter key: Chameleon.Component.Key
    /// - Returns: ChameleonWatcher
    @discardableResult public func register(_ key: Chameleon.Component.Key) -> Self {
        guard let base = base else { return self }
        guard _availableKeys.keys.contains(key) else {
            for (_key, _) in _availableKeys {
                if Chameleon.Component.Key.custom(_key, base.tag) == key {
                    _registerPool.insert(key)
                    break
                }
            }
            return self
        }
        _registerPool.insert(key)
        return self
    }

    /// register a set of key
    ///
    /// - Parameter keys: Chameleon.Component.Key
    /// - Returns: ChameleonWatcher
    @discardableResult public func registerSet(_ keys: Set<Chameleon.Component.Key>) -> Self {
        guard let base = base else { return self }
        for key in keys {
            guard _availableKeys.keys.contains(key) else {
                for (_key, _) in _availableKeys {
                    if Chameleon.Component.Key.custom(_key, base.tag) == key {
                        _registerPool.insert(key)
                        break
                    }
                }
                continue
            }
            _registerPool.insert(key)
        }
        return self
    }
    /// Apply current Skin immediately
    public func apply() { handleSkinChanged(animated: false, tag: base?.tag ?? 0) }
}
