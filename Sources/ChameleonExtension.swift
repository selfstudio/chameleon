//
//  ColorExtension.swift
//  Chameleon
//
//  Created by lincolnlaw on 2017/6/27.
//  Copyright © 2017年 lincolnlaw. All rights reserved.
//

import UIKit

// MARK: - FileManager
extension FileManager {
    func createDirectoryIfNotExist(at path: String) {
        guard fileExists(atPath: path) == false else { return }
        try? createDirectory(atPath: path, withIntermediateDirectories: true, attributes: nil)
    }
}

// MARK: - UIImage
extension UIImage {
    func store(with skin: Chameleon.Skin, component key: String, tag: Int? = nil) -> String {
        var tagString = ""
        if let t = tag { tagString = "|\(t)" }
        let path = "\(Chameleon.mediaFolder(for: skin.name))/\(key)\(tagString)"
        let data = self.pngData()
        let url = URL(fileURLWithPath: path)
        try? data?.write(to: url)
        return tagString
    }

    static func from(skin name: String, key: String, tag: Int? = nil) -> UIImage? {
        var tagString = ""
        if let t = tag { tagString = "|\(t)" }
        let style = Chameleon.Skin.Style.from(raw: name)
        let path = "\(Chameleon.mediaFolder(for: style))/\(key)\(tagString)"
        return UIImage(contentsOfFile: path)
    }
}

// MARK: - UIFont
extension UIFont {
    var json: String { return "\(pointSize)|\(fontName)" }

    static func from(rawValue: String) -> UIFont? {
        let values = rawValue.components(separatedBy: "|")
        if let sizeRaw = values.first, let size = Float(sizeRaw), let name = values.last {
            return UIFont(name: name, size: CGFloat(size))
        }
        return nil
    }
}

// MARK: - String
extension String {
    private func deal(usingStringKey: Bool) -> ([NSAttributedString.Key: Any], [String: Any]) {
        var dict: [NSAttributedString.Key: Any] = [:]
        var stringDict: [String: Any] = [:]
        if let data = data(using: .utf8), let json = (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)) as? [String: String] {
            for (raw, valueRaw) in json {
                let key = NSAttributedString.Key(rawValue: raw)
                let colorKeys: [NSAttributedString.Key] = [.foregroundColor, .backgroundColor, .strokeColor, .underlineColor, .strikethroughColor]
                let intKeys: [NSAttributedString.Key] = [.ligature, .strikethroughStyle, .underlineStyle]
                let floatKeys: [NSAttributedString.Key] = [.kern, .strokeWidth, .baselineOffset, .obliqueness, .expansion]
                if colorKeys.contains(key), let final = UIColor(rgba: valueRaw) {
                    usingStringKey ? (stringDict[raw] = final) : (dict[key] = final)
                }
                if intKeys.contains(key), let final = Int(valueRaw) {
                    usingStringKey ? (stringDict[raw] = final) : (dict[key] = final)
                }
                if floatKeys.contains(key), let final = Float(valueRaw) {
                    usingStringKey ? (stringDict[raw] = final) : (dict[key] = final)
                }
            }
        }
        return (dict, stringDict)
    }

    var stringDict: [String: Any] { return deal(usingStringKey: true).1 }

    var keyDict: [NSAttributedString.Key: Any] { return deal(usingStringKey: true).0 }

    var toInt: Int? { return Int(self) }
    var toCGFloat: CGFloat? { if let f = Float(self) { return CGFloat(f) } else { return nil } }
}

// MARK: - Notification
extension Notification.Name {
    public static let ChameleonSkinChanged: Notification.Name = Notification.Name(rawValue: "ChameleonSkinChanged")
}

// MARK: - UIColor
/**
 MissingHashMarkAsPrefix:   "Invalid RGB string, missing '#' as prefix"
 UnableToScanHexValue:      "Scan hex error"
 MismatchedHexStringLength: "Invalid RGB string, number of characters after '#' should be either 3, 4, 6 or 8"
 */
public enum UIColorInputError: Error {
    case missingHashMarkAsPrefix, unableToScanHexValue, mismatchedHexStringLength
}

extension UIColor {
    /**
     The shorthand three-digit hexadecimal representation of color.
     #RGB defines to the color #RRGGBB.

     - parameter hex3: Three-digit hexadecimal value.
     - parameter alpha: 0.0 - 1.0. The default is 1.0.
     */
    public convenience init(hex3: UInt16, alpha: CGFloat = 1) {
        let divisor = CGFloat(15)
        let red = CGFloat((hex3 & 0xF00) >> 8) / divisor
        let green = CGFloat((hex3 & 0x0F0) >> 4) / divisor
        let blue = CGFloat(hex3 & 0x00F) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }

    /**
     The shorthand four-digit hexadecimal representation of color with alpha.
     #RGBA defines to the color #RRGGBBAA.

     - parameter hex4: Four-digit hexadecimal value.
     */
    public convenience init(hex4: UInt16) {
        let divisor = CGFloat(15)
        let red = CGFloat((hex4 & 0xF000) >> 12) / divisor
        let green = CGFloat((hex4 & 0x0F00) >> 8) / divisor
        let blue = CGFloat((hex4 & 0x00F0) >> 4) / divisor
        let alpha = CGFloat(hex4 & 0x000F) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }

    /**
     The six-digit hexadecimal representation of color of the form #RRGGBB.

     - parameter hex6: Six-digit hexadecimal value.
     */
    public convenience init(hex6: UInt32, alpha: CGFloat = 1) {
        let divisor = CGFloat(255)
        let red = CGFloat((hex6 & 0xFF0000) >> 16) / divisor
        let green = CGFloat((hex6 & 0x00FF00) >> 8) / divisor
        let blue = CGFloat(hex6 & 0x0000FF) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }

    /**
     The six-digit hexadecimal representation of color with alpha of the form #RRGGBBAA.

     - parameter hex8: Eight-digit hexadecimal value.
     */
    public convenience init(hex8: UInt32) {
        let divisor = CGFloat(255)
        let red = CGFloat((hex8 & 0xFF00_0000) >> 24) / divisor
        let green = CGFloat((hex8 & 0x00FF_0000) >> 16) / divisor
        let blue = CGFloat((hex8 & 0x0000_FF00) >> 8) / divisor
        let alpha = CGFloat(hex8 & 0x0000_00FF) / divisor
        self.init(red: red, green: green, blue: blue, alpha: alpha)
    }

    /**
     The rgba string representation of color with alpha of the form #RRGGBBAA/#RRGGBB, throws error.

     - parameter rgba: String value.
     */
    public convenience init(rgba_throws rgba: String) throws {
        guard rgba.hasPrefix("#") else {
            throw UIColorInputError.missingHashMarkAsPrefix
        }
        let start = rgba.index(rgba.startIndex, offsetBy: 1)
        let hexString: String = String(rgba[start...])
        var hexValue: UInt32 = 0

        guard Scanner(string: hexString).scanHexInt32(&hexValue) else {
            throw UIColorInputError.unableToScanHexValue
        }

        switch hexString.count {
        case 3: self.init(hex3: UInt16(hexValue))
        case 4: self.init(hex4: UInt16(hexValue))
        case 6: self.init(hex6: hexValue)
        case 8: self.init(hex8: hexValue)
        default:
            throw UIColorInputError.mismatchedHexStringLength
        }
    }

    public convenience init?(rgba: String) {
        guard let color = try? UIColor(rgba_throws: rgba) else { return nil }
        self.init(cgColor: color.cgColor)
    }

    /**
     Hex string of a UIColor instance.

     - parameter rgba: Whether the alpha should be included.
     */
    public func hexString(_ includeAlpha: Bool) -> String {
        var r: CGFloat = 0
        var g: CGFloat = 0
        var b: CGFloat = 0
        var a: CGFloat = 0
        getRed(&r, green: &g, blue: &b, alpha: &a)

        if includeAlpha {
            return String(format: "#%02X%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255), Int(a * 255))
        } else {
            return String(format: "#%02X%02X%02X", Int(r * 255), Int(g * 255), Int(b * 255))
        }
    }

    open override var description: String {
        return self.hexString(true)
    }

    open override var debugDescription: String {
        return hexString(true)
    }
}
