//
//  Chameleon+UIKit.swift
//  Chameleon
//
//  Created by lincolnlaw on 2017/7/7.
//  Copyright © 2017年 lincolnlaw. All rights reserved.
//

import Foundation

// MARK: - UIKit Extension
// MARK: -
// MARK: - kChameleonTag
private var kChameleonTag = "Chameleon.Tag"
// MARK: - UIViewController
extension UIViewController: ChameleonWizard {
    public var tag: Int {
        get { return objc_getAssociatedObject(self, &kChameleonTag) as? Int ?? 0 }
        set { objc_setAssociatedObject(self, &kChameleonTag, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }
}

// MARK: -
// MARK: - UIView
extension UIView: ChameleonWizard {

    public var chameleonInfo: [Chameleon.Component.Key: Selector] {
        var map: [Chameleon.Component.Key: Selector] = [:]
        if isKind(of: UIView.self) {
            map[.backgroundColor] = #selector(UIView.ch_backgroundColor(_:value:))
            map[.tintColor] = #selector(UIView.ch_tintColor(_:value:))
            map[.alpha] = #selector(UIView.ch_alpha(_:tagValue:))
        }
        if isKind(of: UIButton.self) {
            let bgImgSel = #selector(UIButton.ch_backgroundImage(_:for:))
            let imgSel = #selector(UIButton.ch_image)
            let titleColorSel = #selector(UIButton.ch_titltColor)
            map[.backgroundImageForStateNormal] = bgImgSel
            map[.backgroundImageForStateHighlighted] = bgImgSel
            map[.backgroundImageForStateSelected] = bgImgSel
            map[.backgroundImageForStateDisabled] = bgImgSel
            map[.imageForStateNormal] = imgSel
            map[.imageForStateHighlighted] = imgSel
            map[.imageForStateSelected] = imgSel
            map[.imageForStateDisabled] = imgSel
            map[.titleColorForStateNormal] = titleColorSel
            map[.titleColorForStateHighlighted] = titleColorSel
            map[.titleColorForStateSelected] = titleColorSel
            map[.titleColorForStateDisabled] = titleColorSel
        }
        if isKind(of: UIActivityIndicatorView.self) {
            map[.activityIndicatorViewStyle] = #selector(UIActivityIndicatorView.ch_activityIndicatorViewStyle(_:value:))
        }
        if isKind(of: UINavigationBar.self) {
            map[.barStyle] = #selector(UINavigationBar.ch_barStyle(_:value:))
            map[.navigationbarStyle] = #selector(UINavigationBar.ch_barStyle(_:value:))
            map[.navigationbarTintColor] = #selector(UINavigationBar.ch_tintColor(_:value:))
            map[.barTintColor] = #selector(UINavigationBar.ch_barTintColor(_:value:))
            map[.navigationbarBarTintColor] = #selector(UINavigationBar.ch_barTintColor(_:value:))
            map[.titleTextAttributes] = #selector(UINavigationBar.ch_titleTextAttributes(_:value:))
        }
        if isKind(of: UITabBar.self) {
            map[.barStyle] = #selector(UITabBar.ch_barStyle(_:value:))
            map[.tabbarStyle] = #selector(UITabBar.ch_barStyle(_:value:))
            map[.tabbarTintColor] = #selector(UITabBar.ch_tintColor)
            map[.barTintColor] = #selector(UITabBar.ch_barTintColor(_:value:))
            map[.tabbarBarTintColor] = #selector(UITabBar.ch_barTintColor(_:value:))
        }
        if isKind(of: UIToolbar.self) {
            map[.barStyle] = #selector(UIToolbar.ch_barStyle(_:value:))
            map[.toolbarStyle] = #selector(UIToolbar.ch_barStyle(_:value:))
            map[.toolbarTintColor] = #selector(UIToolbar.ch_tintColor(_:value:))
            map[.barTintColor] = #selector(UIToolbar.ch_barTintColor(_:value:))
            map[.toolbarBarTintColor] = #selector(UIToolbar.ch_barTintColor(_:value:))
        }
        if isKind(of: UISearchBar.self) {
            map[.barStyle] = #selector(UISearchBar.ch_barStyle(_:value:))
            map[.searchbarStyle] = #selector(UISearchBar.ch_barStyle(_:value:))
            map[.keyboardAppearance] = #selector(UISearchBar.ch_keyboardAppearance(_:value:))
            map[.searchbarTintColor] = #selector(UISearchBar.ch_tintColor(_:value:))
            map[.barTintColor] = #selector(UISearchBar.ch_barTintColor(_:value:))
            map[.searchbarBarTintColor] = #selector(UISearchBar.ch_barTintColor(_:value:))
        }
        if isKind(of: UIPageControl.self) {
            map[.pageIndicatorTintColor] = #selector(UIPageControl.ch_pageIndicatorTintColor(_:value:))
            map[.currentPageIndicatorTintColor] = #selector(UIPageControl.ch_currentPageIndicatorTintColor(_:value:))
        }
        if isKind(of: UILabel.self) {
            let textColorSel = #selector(UILabel.ch_textColor(_:value:))
            let fontSel = #selector(UILabel.ch_font(_:value:))
            map[.font] = fontSel
            map[.labelFont] = fontSel
            map[.textColor] = textColorSel
            map[.labelTextColor] = textColorSel
            map[.highlightedTextColor] = #selector(UILabel.ch_highlightedTextColor(_:value:))
            map[.shadowColor] = #selector(UILabel.ch_shadowColor(_:value:))
            map[.title] = textColorSel
            map[.subTitle] = textColorSel
            map[.body] = textColorSel
            map[.footer] = textColorSel
        }
        if isKind(of: UITextView.self) {
            let textColorSel = #selector(UITextView.ch_textColor(_:value:))
            let fontSel = #selector(UITextView.ch_font(_:value:))
            map[.font] = fontSel
            map[.textViewFont] = fontSel
            map[.textColor] = textColorSel
            map[.textViewTextColor] = textColorSel
            map[.body] = textColorSel
            map[.keyboardAppearance] = #selector(UITextView.ch_keyboardAppearance(_:value:))
            map[.textViewKeyboardAppearance] = #selector(UITextView.ch_keyboardAppearance(_:value:))
        }
        if isKind(of: UITextField.self) {
            let textColorSel = #selector(UITextField.ch_textColor(_:value:))
            let fontSel = #selector(UITextField.ch_font(_:value:))
            map[.font] = fontSel
            map[.textFieldFont] = fontSel
            map[.textColor] = textColorSel
            map[.textFieldTextColor] = textColorSel
            map[.keyboardAppearance] = #selector(UITextField.ch_keyboardAppearance(_:value:))
            map[.textFieldKeyboardAppearance] = #selector(UITextField.ch_keyboardAppearance(_:value:))
        }
        if isKind(of: UIImageView.self) {
            map[.image] = #selector(UIImageView.ch_image(_:value:))
        }
        if isKind(of: UISlider.self) {
            map[.maximumTrackTintColor] = #selector(UISlider.ch_maximumTrackTintColor(_:value:))
            map[.minimumTrackTintColor] = #selector(UISlider.ch_minimumTrackTintColor(_:value:))
            map[.thumbTintColor] = #selector(UISlider.ch_thumbTintColor(_:value:))
        }
        if isKind(of: UISwitch.self) {
            map[.onTintColor] = #selector(UISwitch.ch_onTintColor(_:value:))
        }
        if isKind(of: UIProgressView.self) {
            map[.progressTintColor] = #selector(UIProgressView.ch_progressTintColor(_:value:))
            map[.trackTintColor] = #selector(UIProgressView.ch_trackTintColor(_:value:))
        }
        if isKind(of: UITableView.self) {
            map[.separatorColor] = #selector(UITableView.ch_separatorColor(_:value:))
        }
        return map
    }

    // MARK: - Custom Selector
    @objc func ch_alpha(_ value: String, tagValue: String?) {
        guard let result = value.toCGFloat, isMyTag(tagValue) else { return }
        alpha = CGFloat(result)
    }

    @objc func ch_backgroundColor(_ color: UIColor?, value: String?) {
        guard isMyTag(value) else { return }
        backgroundColor = color
    }

    @objc func ch_tintColor(_ color: UIColor?, value: String?) {
        guard isMyTag(value) else { return }
        tintColor = color
    }
}

// MARK: - UIButton
extension UIButton {

    @objc func ch_titltColor(_ color: UIColor?, for value: String) {
        let (_state, canSet) = isMyTagForState(value)
        guard let targetState = _state, canSet == true else { return }
        setTitleColor(color, for: targetState)
    }

    @objc func ch_image(_ image: UIImage?, for value: String) {
        let (_state, canSet) = isMyTagForState(value)
        guard let targetState = _state, canSet == true else { return }
        setImage(image, for: targetState)
    }

    @objc func ch_backgroundImage(_ image: UIImage?, for value: String) {
        let (_state, canSet) = isMyTagForState(value)
        guard let targetState = _state, canSet == true else { return }
        setBackgroundImage(image, for: targetState)
    }
}

// MARK: - UIActivityIndicatorView
extension UIActivityIndicatorView {
    @objc func ch_activityIndicatorViewStyle(_ raw: String, value: String?) {
        guard isMyTag(value), let value = Int(raw), let style = UIActivityIndicatorView.Style(rawValue: value) else { return }
        self.style = style
    }
}

// MARK: - UINavigationBar
extension UINavigationBar {

    @objc func ch_barStyle(_ raw: String, value: String?) {
        guard isMyTag(value), let v = Int(raw), let style = UIBarStyle(rawValue: v) else { return }
        barStyle = style
    }

    @objc func ch_barTintColor(_ color: UIColor, value: String?) {
        guard isMyTag(value) else { return }
        barTintColor = color
    }

    @objc func ch_titleTextAttributes(_ json: [NSAttributedString.Key: Any], value: String?) {
        guard isMyTag(value) else { return }
        titleTextAttributes = json
    }
}

// MARK: - UITabBar
extension UITabBar {
    @objc func ch_barStyle(_ raw: String, value: String?) {
        guard isMyTag(value), let v = Int(raw), let style = UIBarStyle(rawValue: v) else { return }
        barStyle = style
    }

    @objc func ch_barTintColor(_ color: UIColor, value: String?) {
        guard isMyTag(value) else { return }
        barTintColor = color
    }
}

// MARK: - UIToolbar
extension UIToolbar {
    @objc func ch_barStyle(_ raw: String, value: String?) {
        guard isMyTag(value), let v = Int(raw), let style = UIBarStyle(rawValue: v) else { return }
        barStyle = style
    }

    @objc func ch_barTintColor(_ color: UIColor, value: String?) {
        guard isMyTag(value) else { return }
        barTintColor = color
    }
}

// MARK: - UISearchBar
extension UISearchBar {
    @objc func ch_barStyle(_ raw: String, value: String?) {
        guard isMyTag(value), let v = Int(raw), let style = UIBarStyle(rawValue: v) else { return }
        barStyle = style
    }

    @objc func ch_keyboardAppearance(_ raw: String, value: String?) {
        guard isMyTag(value), let v = Int(raw), let style = UIKeyboardAppearance(rawValue: v) else { return }
        keyboardAppearance = style
    }

    @objc func ch_barTintColor(_ color: UIColor, value: String?) {
        guard isMyTag(value) else { return }
        barTintColor = color
    }
}

// MARK: - UIPageControl
extension UIPageControl {
    @objc func ch_pageIndicatorTintColor(_ color: UIColor?, value: String?) {
        guard isMyTag(value) else { return }
        pageIndicatorTintColor = color
    }

    @objc func ch_currentPageIndicatorTintColor(_ color: UIColor?, value: String?) {
        guard isMyTag(value) else { return }
        currentPageIndicatorTintColor = color
    }
}

// MARK: - UILabel
extension UILabel {
    @objc func ch_font(_ raw: UIFont, value: String?) {
        guard isMyTag(value) else { return }
        font = raw
    }

    @objc func ch_textColor(_ color: UIColor, value: String?) {
        guard isMyTag(value) else { return }
        textColor = color
    }

    @objc func ch_highlightedTextColor(_ color: UIColor, value: String?) {
        guard isMyTag(value) else { return }
        highlightedTextColor = color
    }

    @objc func ch_shadowColor(_ color: UIColor?, value: String?) {
        guard isMyTag(value) else { return }
        shadowColor = color
    }
}

// MARK: - UITextField
extension UITextField {
    @objc func ch_keyboardAppearance(_ raw: String, value: String?) {
        guard isMyTag(value), let v = Int(raw), let style = UIKeyboardAppearance(rawValue: v) else { return }
        keyboardAppearance = style
    }

    @objc func ch_font(_ raw: UIFont, value: String?) {
        guard isMyTag(value) else { return }
        font = raw
    }

    @objc func ch_textColor(_ color: UIColor, value: String?) {
        guard isMyTag(value) else { return }
        textColor = color
    }
}

// MARK: - UITextView
extension UITextView {
    @objc func ch_font(_ raw: UIFont, value: String?) {
        guard isMyTag(value) else { return }
        font = raw
    }

    @objc func ch_textColor(_ color: UIColor, value: String?) {
        guard isMyTag(value) else { return }
        textColor = color
    }

    @objc func ch_keyboardAppearance(_ raw: String, value: String?) {
        guard isMyTag(value), let value = Int(raw), let style = UIKeyboardAppearance(rawValue: value) else { return }
        keyboardAppearance = style
    }
}

// MARK: - UIImageView
extension UIImageView {
    @objc func ch_image(_ img: UIImage?, value: String?) {
        guard isMyTag(value) else { return }
        image = img
    }
}

extension UISlider {
    @objc func ch_maximumTrackTintColor(_ color: UIColor?, value: String?) {
        guard isMyTag(value) else { return }
        maximumTrackTintColor = color
    }

    @objc func ch_minimumTrackTintColor(_ color: UIColor?, value: String?) {
        guard isMyTag(value) else { return }
        minimumTrackTintColor = color
    }

    @objc func ch_thumbTintColor(_ color: UIColor?, value: String?) {
        guard isMyTag(value) else { return }
        thumbTintColor = color
    }
}

extension UISwitch {
    @objc func ch_onTintColor(_ color: UIColor?, value: String?) {
        guard isMyTag(value) else { return }
        onTintColor = color
    }
}

extension UIProgressView {
    @objc func ch_progressTintColor(_ color: UIColor?, value: String?) {
        guard isMyTag(value) else { return }
        progressTintColor = color
    }

    @objc func ch_trackTintColor(_ color: UIColor?, value: String?) {
        guard isMyTag(value) else { return }
        trackTintColor = color
    }
}

extension UITableView {
    @objc func ch_separatorColor(_ color: UIColor?, value: String?) {
        guard isMyTag(value) else { return }
        separatorColor = color
    }
}

// MARK: -
// MARK: - CALayer
extension CALayer: ChameleonWizard {

    public var tag: Int {
        get { return objc_getAssociatedObject(self, &kChameleonTag) as? Int ?? 0 }
        set { objc_setAssociatedObject(self, &kChameleonTag, newValue, .OBJC_ASSOCIATION_RETAIN_NONATOMIC) }
    }

    public var chameleonInfo: [Chameleon.Component.Key: Selector] {
        var map: [Chameleon.Component.Key: Selector] = [:]
        if isKind(of: CALayer.self) {
            map[.backgroundColor] = #selector(CALayer.ch_backgroundColor)
            map[.borderWidth] = #selector(CALayer.ch_borderWidth)
            map[.borderColor] = #selector(CALayer.ch_borderColor)
            map[.shadowColor] = #selector(CALayer.ch_shadowColor)
        }
        if isKind(of: CAShapeLayer.self) {
            map[.strokeColor] = #selector(CAShapeLayer.ch_strokeColor(_:value:))
            map[.fillColor] = #selector(CAShapeLayer.ch_fillColor(_:value:))
        }
        return map
    }

    @objc func ch_borderWidth(_ raw: String, value: String?) {
        guard let result = Float(raw), isMyTag(value) else { return }
        borderWidth = CGFloat(result)
    }

    @objc func ch_backgroundColor(_ color: UIColor, value: String?) {
        guard isMyTag(value) else { return }
        backgroundColor = color.cgColor
    }

    @objc func ch_borderColor(_ color: UIColor, value: String?) {
        guard isMyTag(value) else { return }
        borderColor = color.cgColor
    }

    @objc func ch_shadowColor(_ color: UIColor, value: String?) {
        guard isMyTag(value) else { return }
        shadowColor = color.cgColor
    }
}

extension CAShapeLayer {
    @objc func ch_strokeColor(_ color: UIColor, value: String?) {
        guard isMyTag(value) else { return }
        strokeColor = color.cgColor
    }

    @objc func ch_fillColor(_ color: UIColor, value: String?) {
        guard isMyTag(value) else { return }
        fillColor = color.cgColor
    }
}

// MARK: -
// MARK: - UITabBarItem
extension UITabBarItem: ChameleonWizard {
    public var chameleonInfo: [Chameleon.Component.Key: Selector] {
        let titleAttSel = #selector(UITabBarItem.ch_titleTextAttributes)
        let info: [Chameleon.Component.Key: Selector] = [
            .titleTextAttributesForStateNormal: titleAttSel,
            .titleTextAttributesForStateHighlighted: titleAttSel,
            .titleTextAttributesForStateSelected: titleAttSel,
            .titleTextAttributesForStateDisabled: titleAttSel,
            .tabbarImage: #selector(UITabBarItem.ch_image(_:for:)),
            .tabbarSelectedImage: #selector(UITabBarItem.ch_selectedImage(_:for:)),
        ]
        return info
    }

    @objc func ch_titleTextAttributes(_ json: [NSAttributedString.Key: Any], for value: String) {
        let (_state, canSet) = isMyTagForState(value)
        guard let targetState = _state, canSet == true else { return }
        CATransaction.begin()
        CATransaction.setValue(true, forKey: kCATransactionDisableActions)
        setTitleTextAttributes(json, for: targetState)
        CATransaction.commit()
    }

    @objc func ch_image(_ img: UIImage?, for tagRaw: String) {
        guard isMyTag(tagRaw) else { return }
        image = img
    }

    @objc func ch_selectedImage(_ img: UIImage?, for tagRaw: String) {
        guard isMyTag(tagRaw) else { return }
        selectedImage = img
    }
}
