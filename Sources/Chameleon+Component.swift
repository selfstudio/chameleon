//
//  Chameleon+Component.swift
//  Chameleon
//
//  Created by Lincoln Law on 2019/1/9.
//  Copyright © 2019 lincolnlaw. All rights reserved.
//

import Foundation

extension Chameleon {
    public indirect enum Component: Hashable {
        case title(UIColor)
        case subTitle(UIColor)
        case body(UIColor)
        case footer(UIColor)
        case backgroundColor(UIColor?)
        case tintColor(UIColor)
        case navigationbarTintColor(UIColor)
        case tabbarTintColor(UIColor)
        case toolbarTintColor(UIColor)
        case searchbarTintColor(UIColor)
        case alpha(CGFloat)
        case font(UIFont)
        case labelFont(UIFont)
        case textFieldFont(UIFont)
        case textViewFont(UIFont)
        case textColor(UIColor)
        case labelTextColor(UIColor)
        case textFieldTextColor(UIColor)
        case textViewTextColor(UIColor)
        case highlightedTextColor(UIColor)
        case shadowColor(UIColor?)
        case barStyle(UIBarStyle)
        case navigationbarStyle(UIBarStyle)
        case tabbarStyle(UIBarStyle)
        case toolbarStyle(UIBarStyle)
        case searchbarStyle(UIBarStyle)
        case barTintColor(UIColor?)
        case navigationbarBarTintColor(UIColor?)
        case tabbarBarTintColor(UIColor?)
        case toolbarBarTintColor(UIColor?)
        case searchbarBarTintColor(UIColor?)
        case titleTextAttributes([String: Any])
        case titleTextAttributesForState([NSAttributedString.Key: Any], UIControl.State)
        case separatorColor(UIColor?)
        case keyboardAppearance(UIKeyboardAppearance)
        case textFieldKeyboardAppearance(UIKeyboardAppearance)
        case textViewKeyboardAppearance(UIKeyboardAppearance)
        case onTintColor(UIColor?)
        case thumbTintColor(UIColor?)
        case minimumTrackTintColor(UIColor?)
        case maximumTrackTintColor(UIColor?)
        case progressTintColor(UIColor?)
        case trackTintColor(UIColor?)
        case pageIndicatorTintColor(UIColor?)
        case currentPageIndicatorTintColor(UIColor?)
        case image(UIImage?)
        case tabbarImage(UIImage?)
        case tabbarSelectedImage(UIImage?)
        case activityIndicatorViewStyle(UIActivityIndicatorView.Style)
        case imageForState(UIImage?, UIControl.State)
        case backgroundImageForState(UIImage?, UIControl.State)
        case titleColorForState(UIColor?, UIControl.State)
        case borderWidth(CGFloat)
        case borderColor(UIColor?)
        case strokeColor(UIColor?)
        case fillColor(UIColor?)
        case preferredStatusBarStyle(UIStatusBarStyle)
        case custom(Component, Int)
        
        public var hashValue: Int {
            func hash(for state: UIControl.State) -> Int { return key.hashValue + Int(state.rawValue) }
            switch self {
            case let .titleColorForState(_, state): return hash(for: state)
            case let .imageForState(_, state): return hash(for: state)
            case let .backgroundImageForState(_, state): return hash(for: state)
            case let .titleTextAttributesForState(_, state): return hash(for: state)
            default: return key.hashValue
            }
        }
        
        public static func ==(lhs: Chameleon.Component, rhs: Chameleon.Component) -> Bool { return lhs.hashValue == rhs.hashValue }
        
        // MARK: Codable
        public static func instance(from json: [String: String], style: Skin.Style) -> Set<Component> {
            var ret: Set<Component> = []
            let styleName = style.name
            
            func action(from key: Component.Key, vl: String) -> Component? {
                var r: Component?
                switch key {
                case .activityIndicatorViewStyle: if let v = Int(vl), let type = UIActivityIndicatorView.Style(rawValue: v) { r = .activityIndicatorViewStyle(type) }
                case .alpha: if let v = Float(vl) { r = .alpha(CGFloat(v)) }
                case .backgroundColor: r = .backgroundColor(UIColor(rgba: vl))
                case .backgroundImageForStateNormal: r = .backgroundImageForState(UIImage.from(skin: styleName, key: key.value), .normal)
                case .backgroundImageForStateHighlighted: r = .backgroundImageForState(UIImage.from(skin: styleName, key: key.value), .highlighted)
                case .backgroundImageForStateSelected: r = .backgroundImageForState(UIImage.from(skin: styleName, key: key.value), .selected)
                case .backgroundImageForStateDisabled: r = .backgroundImageForState(UIImage.from(skin: styleName, key: key.value), .disabled)
                case .barStyle: if let v = Int(vl), let style = UIBarStyle(rawValue: v) { r = .barStyle(style) }
                case .barTintColor: r = .barTintColor(UIColor(rgba: vl))
                case .body: if let v = UIColor(rgba: vl) { r = .body(v) }
                case .borderColor: r = .borderColor(UIColor(rgba: vl))
                case .borderWidth: if let v = Float(vl) { r = .borderWidth(CGFloat(v)) }
                case .currentPageIndicatorTintColor: r = .currentPageIndicatorTintColor(UIColor(rgba: vl))
                case .font: if let v = UIFont.from(rawValue: vl) { r = .font(v) }
                case .footer: if let v = UIColor(rgba: vl) { r = .footer(v) }
                case .highlightedTextColor: if let v = UIColor(rgba: vl) { r = .highlightedTextColor(v) }
                case .image: r = .image(UIImage.from(skin: styleName, key: key.value))
                case .tabbarImage: r = .tabbarImage(UIImage.from(skin: styleName, key: key.value))
                case .tabbarSelectedImage: r = .tabbarSelectedImage(UIImage.from(skin: styleName, key: key.value))
                case .imageForStateNormal: r = .imageForState(UIImage.from(skin: styleName, key: key.value), .normal)
                case .imageForStateHighlighted: r = .imageForState(UIImage.from(skin: styleName, key: key.value), .highlighted)
                case .imageForStateSelected: r = .imageForState(UIImage.from(skin: styleName, key: key.value), .selected)
                case .imageForStateDisabled: r = .imageForState(UIImage.from(skin: styleName, key: key.value), .disabled)
                case .keyboardAppearance: if let v = Int(vl), let type = UIKeyboardAppearance(rawValue: v) { r = .keyboardAppearance(type) }
                case .labelFont: if let v = UIFont.from(rawValue: vl) { r = .labelFont(v) }
                case .labelTextColor: if let v = UIColor(rgba: vl) { r = .labelTextColor(v) }
                case .maximumTrackTintColor: r = .maximumTrackTintColor(UIColor(rgba: vl))
                case .minimumTrackTintColor: r = .minimumTrackTintColor(UIColor(rgba: vl))
                case .navigationbarStyle: if let v = Int(vl), let type = UIBarStyle(rawValue: v) { r = .navigationbarStyle(type) }
                case .navigationbarTintColor: if let v = UIColor(rgba: vl) { r = .navigationbarTintColor(v) }
                case .navigationbarBarTintColor: r = .navigationbarBarTintColor(UIColor(rgba: vl))
                case .onTintColor: r = .onTintColor(UIColor(rgba: vl))
                case .pageIndicatorTintColor: r = .pageIndicatorTintColor(UIColor(rgba: vl))
                case .preferredStatusBarStyle: if let v = Int(vl), let type = UIStatusBarStyle(rawValue: v) { r = .preferredStatusBarStyle(type) }
                case .progressTintColor: r = .progressTintColor(UIColor(rgba: vl))
                case .searchbarStyle: if let v = Int(vl), let type = UIBarStyle(rawValue: v) { r = .searchbarStyle(type) }
                case .searchbarBarTintColor: r = .searchbarBarTintColor(UIColor(rgba: vl))
                case .searchbarTintColor: if let v = UIColor(rgba: vl) { r = .searchbarTintColor(v) }
                case .separatorColor: r = .separatorColor(UIColor(rgba: vl))
                case .shadowColor: r = .shadowColor(UIColor(rgba: vl))
                case .subTitle: if let v = UIColor(rgba: vl) { r = .subTitle(v) }
                case .tabbarStyle: if let v = Int(vl), let type = UIBarStyle(rawValue: v) { r = .searchbarStyle(type) }
                case .tabbarBarTintColor: r = .tabbarBarTintColor(UIColor(rgba: vl))
                case .tabbarTintColor: if let v = UIColor(rgba: vl) { r = .tabbarTintColor(v) }
                case .textColor: if let v = UIColor(rgba: vl) { r = .textColor(v) }
                case .textFieldFont: if let v = UIFont.from(rawValue: vl) { r = .textFieldFont(v) }
                case .textFieldKeyboardAppearance: if let v = Int(vl), let type = UIKeyboardAppearance(rawValue: v) { r = .textFieldKeyboardAppearance(type) }
                case .textFieldTextColor: if let v = UIColor(rgba: vl) { r = .textFieldTextColor(v) }
                case .textViewFont: if let v = UIFont.from(rawValue: vl) { r = .textViewFont(v) }
                case .textViewKeyboardAppearance: if let v = Int(vl), let type = UIKeyboardAppearance(rawValue: v) { r = .textViewKeyboardAppearance(type) }
                case .textViewTextColor: if let v = UIColor(rgba: vl) { r = .textViewTextColor(v) }
                case .thumbTintColor: r = .thumbTintColor(UIColor(rgba: vl))
                case .tintColor: if let v = UIColor(rgba: vl) { r = .tintColor(v) }
                case .title: if let v = UIColor(rgba: vl) { r = .title(v) }
                case .titleColorForStateNormal: r = .titleColorForState(UIColor(rgba: vl), .normal)
                case .titleColorForStateHighlighted: r = .titleColorForState(UIColor(rgba: vl), .highlighted)
                case .titleColorForStateSelected: r = .titleColorForState(UIColor(rgba: vl), .selected)
                case .titleColorForStateDisabled: r = .titleColorForState(UIColor(rgba: vl), .disabled)
                case .titleTextAttributes: r = .titleTextAttributes(vl.stringDict)
                case .titleTextAttributesForStateNormal: r = .titleTextAttributesForState(vl.keyDict, .normal)
                case .titleTextAttributesForStateHighlighted: r = .titleTextAttributesForState(vl.keyDict, .highlighted)
                case .titleTextAttributesForStateSelected: r = .titleTextAttributesForState(vl.keyDict, .selected)
                case .titleTextAttributesForStateDisabled: r = .titleTextAttributesForState(vl.keyDict, .disabled)
                case .toolbarStyle: if let v = Int(vl), let t = UIBarStyle(rawValue: v) { r = .toolbarStyle(t) }
                case .toolbarBarTintColor: r = .toolbarBarTintColor(UIColor(rgba: vl))
                case .toolbarTintColor: if let v = UIColor(rgba: vl) { r = .tintColor(v) }
                case .trackTintColor: r = .trackTintColor(UIColor(rgba: vl))
                case .strokeColor: r = .strokeColor(UIColor(rgba: vl))
                case .fillColor: r = .fillColor(UIColor(rgba: vl))
                case let .custom(key, tag): if let com = action(from: key, vl: vl) { r = .custom(com, tag) }
                }
                return r
            }
            
            for (raw, vl) in json {
                guard let key = Key.from(rawValue: raw), let value = action(from: key, vl: vl) else { continue }
                ret.insert(value)
            }
            return ret
        }
        
        public static func json(for list: Set<Component>, in skin: Skin) -> String {
            var json: String = "{"
            for item in list {
                guard let value = item.json(for: skin) else { continue }
                let needDeal: Set<Component.Key> = [.titleTextAttributes, .titleTextAttributesForStateNormal, .titleTextAttributesForStateHighlighted, .titleTextAttributesForStateSelected, .titleTextAttributesForStateDisabled]
                let deals = needDeal.compactMap({ Optional($0.value) })
                if deals.contains(item.key) {
                    json += "\"\(item.key)\":\(value),"
                } else {
                    json += "\"\(item.key)\":\"\(value)\","
                }
            }
            json += "}"
            return json
        }
        
        public func json(for skin: Skin) -> String? {
            return rawJson(form: self, skin: skin)
        }
        
        func rawJson(form component: Component, skin: Skin) -> String? {
            let key = component.key
            var raw: String?
            switch component {
            case let .activityIndicatorViewStyle(value): raw = "\(value.rawValue)"
            case let .alpha(value): raw = "\(value)"
            case let .backgroundColor(value): raw = value?.hexString(true)
            case let .backgroundImageForState(image, _): raw = image?.store(with: skin, component: key)
            case let .barStyle(value): raw = "\(value.rawValue)"
            case let .barTintColor(value): raw = value?.hexString(true)
            case let .body(value): raw = value.hexString(true)
            case let .borderColor(value): raw = value?.hexString(true)
            case let .borderWidth(value): raw = "\(value)"
            case let .currentPageIndicatorTintColor(value): raw = value?.hexString(true)
            case let .font(font): raw = font.json
            case let .footer(value): raw = value.hexString(true)
            case let .highlightedTextColor(value): raw = value.hexString(true)
            case let .image(image): raw = image?.store(with: skin, component: key) ?? ""
            case let .tabbarImage(image): raw = image?.store(with: skin, component: key) ?? ""
            case let .tabbarSelectedImage(image): raw = image?.store(with: skin, component: key) ?? ""
            case let .imageForState(image, _): raw = image?.store(with: skin, component: key)
            case let .keyboardAppearance(value): raw = "\(value.rawValue)"
            case let .labelFont(font): raw = font.json
            case let .labelTextColor(value): raw = value.hexString(true)
            case let .maximumTrackTintColor(value): raw = value?.hexString(true)
            case let .minimumTrackTintColor(value): raw = value?.hexString(true)
            case let .navigationbarStyle(value): raw = "\(value.rawValue)"
            case let .navigationbarBarTintColor(value): raw = value?.hexString(true)
            case let .navigationbarTintColor(value): raw = value.hexString(true)
            case let .onTintColor(value): raw = value?.hexString(true)
            case let .pageIndicatorTintColor(value): raw = value?.hexString(true)
            case let .preferredStatusBarStyle(value): raw = "\(value.rawValue)"
            case let .progressTintColor(value): raw = value?.hexString(true)
            case let .searchbarStyle(value): raw = "\(value.rawValue)"
            case let .searchbarBarTintColor(value): raw = value?.hexString(true)
            case let .searchbarTintColor(value): raw = value.hexString(true)
            case let .separatorColor(value): raw = value?.hexString(true)
            case let .shadowColor(value): raw = value?.hexString(true)
            case let .subTitle(value): raw = value.hexString(true)
            case let .tabbarStyle(value): raw = "\(value.rawValue)"
            case let .tabbarBarTintColor(value): raw = value?.hexString(true)
            case let .tabbarTintColor(value): raw = value.hexString(true)
            case let .textColor(value): raw = value.hexString(true)
            case let .textFieldFont(font): raw = font.json
            case let .textFieldKeyboardAppearance(value): raw = "\(value.rawValue)"
            case let .textFieldTextColor(value): raw = value.hexString(true)
            case let .textViewFont(font): raw = font.json
            case let .textViewKeyboardAppearance(value): raw = "\(value.rawValue)"
            case let .textViewTextColor(value): raw = value.hexString(true)
            case let .thumbTintColor(value): raw = value?.hexString(true)
            case let .tintColor(value): raw = value.hexString(true)
            case let .title(value): raw = value.hexString(true)
            case let .titleColorForState(color, _): raw = color?.hexString(true)
            case let .titleTextAttributes(dict):
                var total = "{"
                for (key, value) in dict {
                    var val = ""
                    if let font = value as? UIFont {
                        val = font.json
                    } else if let color = value as? UIColor {
                        val = color.hexString(true)
                    } else if let number = value as? Int {
                        val = "\(number)"
                    } else if let number = value as? Float {
                        val = "\(number)"
                    }
                    total += "\"\(key)\":\"\(val)\","
                }
                total += "}"
                raw = total
            case let .titleTextAttributesForState(dict, _):
                var total = "{"
                for (key, value) in dict {
                    var val = ""
                    if let font = value as? UIFont {
                        val = font.json
                    } else if let color = value as? UIColor {
                        val = color.hexString(true)
                    } else if let number = value as? Int {
                        val = "\(number)"
                    } else if let number = value as? Float {
                        val = "\(number)"
                    }
                    total += "\"\(key.rawValue)\":\"\(val)\","
                }
                total += "}"
                raw = total
            case let .toolbarStyle(value): raw = "\(value.rawValue)"
            case let .toolbarBarTintColor(value): raw = value?.hexString(true)
            case let .toolbarTintColor(value): raw = value.hexString(true)
            case let .trackTintColor(value): raw = value?.hexString(true)
            case let .strokeColor(value): raw = value?.hexString(true)
            case let .fillColor(value): raw = value?.hexString(true)
            case let .custom(customComponet, tag): raw = "\(rawJson(form: customComponet, skin: skin) ?? "")|\(tag)"
            }
            return raw
        }
        
        public var key: String { return rawKey(form: self) }
        
        private func rawKey(form component: Component) -> String {
            switch component {
            case .title: return Key.title.value
            case .subTitle: return Key.subTitle.value
            case .body: return Key.body.value
            case .footer: return Key.footer.value
            case .activityIndicatorViewStyle: return Key.activityIndicatorViewStyle.value
            case .alpha: return Key.alpha.value
            case .backgroundColor: return Key.backgroundColor.value
            case let .backgroundImageForState(_, state):
                switch state {
                case .highlighted: return Key.backgroundImageForStateHighlighted.value
                case .selected: return Key.backgroundImageForStateSelected.value
                case .disabled: return Key.backgroundImageForStateDisabled.value
                default: return Key.backgroundImageForStateNormal.value
                }
            case .barStyle: return Key.barStyle.value
            case .navigationbarStyle: return Key.navigationbarStyle.value
            case .tabbarStyle: return Key.tabbarStyle.value
            case .toolbarStyle: return Key.toolbarStyle.value
            case .searchbarStyle: return Key.searchbarStyle.value
            case .barTintColor: return Key.barTintColor.value
            case .navigationbarTintColor: return Key.navigationbarTintColor.value
            case .tabbarTintColor: return Key.tabbarTintColor.value
            case .toolbarTintColor: return Key.toolbarTintColor.value
            case .searchbarTintColor: return Key.searchbarTintColor.value
            case .borderColor: return Key.borderColor.value
            case .borderWidth: return Key.borderWidth.value
            case .currentPageIndicatorTintColor: return Key.currentPageIndicatorTintColor.value
            case .font: return Key.font.value
            case .labelFont: return Key.labelFont.value
            case .textFieldFont: return Key.textFieldFont.value
            case .textViewFont: return Key.textViewFont.value
            case .highlightedTextColor: return Key.highlightedTextColor.value
            case .image: return Key.image.value
            case .tabbarImage: return Key.tabbarImage.value
            case .tabbarSelectedImage: return Key.tabbarSelectedImage.value
            case let .imageForState(_, state):
                switch state {
                case .highlighted: return Key.imageForStateHighlighted.value
                case .selected: return Key.imageForStateSelected.value
                case .disabled: return Key.imageForStateDisabled.value
                default: return Key.imageForStateNormal.value
                }
            case .keyboardAppearance: return Key.keyboardAppearance.value
            case .textFieldKeyboardAppearance: return Key.textFieldKeyboardAppearance.value
            case .textViewKeyboardAppearance: return Key.textViewKeyboardAppearance.value
            case .maximumTrackTintColor: return Key.maximumTrackTintColor.value
            case .minimumTrackTintColor: return Key.minimumTrackTintColor.value
            case .onTintColor: return Key.onTintColor.value
            case .pageIndicatorTintColor: return Key.pageIndicatorTintColor.value
            case .preferredStatusBarStyle: return Key.preferredStatusBarStyle.value
            case .progressTintColor: return Key.progressTintColor.value
            case .separatorColor: return Key.separatorColor.value
            case .shadowColor: return Key.shadowColor.value
            case .textColor: return Key.textColor.value
            case .labelTextColor: return Key.labelTextColor.value
            case .textFieldTextColor: return Key.textFieldTextColor.value
            case .textViewTextColor: return Key.textViewTextColor.value
            case .thumbTintColor: return Key.thumbTintColor.value
            case .tintColor: return Key.tintColor.value
            case let .titleColorForState(_, state):
                switch state {
                case .highlighted: return Key.titleColorForStateHighlighted.value
                case .selected: return Key.titleColorForStateSelected.value
                case .disabled: return Key.titleColorForStateDisabled.value
                default: return Key.titleColorForStateNormal.value
                }
            case .titleTextAttributes: return Key.titleTextAttributes.value
            case let .titleTextAttributesForState(_, state):
                switch state {
                case .highlighted: return Key.titleTextAttributesForStateHighlighted.value
                case .selected: return Key.titleTextAttributesForStateSelected.value
                case .disabled: return Key.titleTextAttributesForStateDisabled.value
                default: return Key.titleTextAttributesForStateNormal.value
                }
            case .trackTintColor: return Key.trackTintColor.value
            case .navigationbarBarTintColor: return Key.navigationbarBarTintColor.value
            case .tabbarBarTintColor: return Key.tabbarBarTintColor.value
            case .toolbarBarTintColor: return Key.toolbarBarTintColor.value
            case .searchbarBarTintColor: return Key.searchbarBarTintColor.value
            case .strokeColor: return Key.strokeColor.value
            case .fillColor: return Key.fillColor.value
            case let .custom(component, tag):
                let cKey = rawKey(form: component)
                if let finalKey = Key.from(rawValue: cKey) {
                    return Key.custom(finalKey, tag).value
                }
                assert(false, "unexpect rawvalue:\(cKey) for Chameleon.Component.Key")
                return ""
            }
        }
        
        public indirect enum Key: Hashable {
            
            static let total: [Key] = [.title, .subTitle, .body, .footer, .backgroundColor, .tintColor, .navigationbarTintColor, .tabbarTintColor, .toolbarTintColor, .searchbarTintColor, .alpha, .font, .labelFont, .textFieldFont, .textViewFont, .textColor, .labelTextColor, .textFieldTextColor, .textViewTextColor, .highlightedTextColor, .shadowColor, .barStyle, .navigationbarStyle, .tabbarStyle, .toolbarStyle, .searchbarStyle, .barTintColor, .navigationbarBarTintColor, .tabbarBarTintColor, .toolbarBarTintColor, .searchbarBarTintColor, .titleTextAttributes, .titleTextAttributesForStateNormal, .titleTextAttributesForStateHighlighted, .titleTextAttributesForStateSelected, .titleTextAttributesForStateDisabled, .separatorColor, .keyboardAppearance, .textFieldKeyboardAppearance, .textViewKeyboardAppearance, .onTintColor, .thumbTintColor, .minimumTrackTintColor, .maximumTrackTintColor, .preferredStatusBarStyle, .progressTintColor, .trackTintColor, .pageIndicatorTintColor, .currentPageIndicatorTintColor, .image, .tabbarImage, .tabbarSelectedImage, .activityIndicatorViewStyle, .imageForStateNormal, .imageForStateHighlighted, .imageForStateSelected, .imageForStateDisabled, .backgroundImageForStateNormal, .backgroundImageForStateHighlighted, .backgroundImageForStateSelected, .backgroundImageForStateDisabled, .titleColorForStateNormal, .titleColorForStateHighlighted, .titleColorForStateSelected, .titleColorForStateDisabled, .borderWidth, .borderColor, .strokeColor, .fillColor]
            
            public var hashValue: Int { return value.hashValue }
            
            public static func ==(lhs: Chameleon.Component.Key, rhs: Chameleon.Component.Key) -> Bool {
                return lhs.value == rhs.value
            }
            
            static func from(rawValue: String) -> Key? {
                if rawValue.contains("|") {
                    let kv = rawValue.components(separatedBy: "|")
                    if let keyRaw = kv.first, let key = Key.from(rawValue: keyRaw), let tagRaw = kv.last, let tag = Int(tagRaw) {
                        return Key.custom(key, tag)
                    }
                } else {
                    for item in total {
                        if rawValue == "\(item)" { return item }
                    }
                }
                return nil
            }
            
            case title
            case subTitle
            case body
            case footer
            case backgroundColor
            case tintColor
            case navigationbarTintColor
            case tabbarTintColor
            case toolbarTintColor
            case searchbarTintColor
            case alpha
            case font
            case labelFont
            case textFieldFont
            case textViewFont
            case textColor
            case labelTextColor
            case textFieldTextColor
            case textViewTextColor
            case highlightedTextColor
            case shadowColor
            case barStyle
            case navigationbarStyle
            case tabbarStyle
            case toolbarStyle
            case searchbarStyle
            case barTintColor
            case navigationbarBarTintColor
            case tabbarBarTintColor
            case toolbarBarTintColor
            case searchbarBarTintColor
            case titleTextAttributes
            case titleTextAttributesForStateNormal
            case titleTextAttributesForStateHighlighted
            case titleTextAttributesForStateSelected
            case titleTextAttributesForStateDisabled
            case separatorColor
            case keyboardAppearance
            case textFieldKeyboardAppearance
            case textViewKeyboardAppearance
            case onTintColor
            case thumbTintColor
            case minimumTrackTintColor
            case maximumTrackTintColor
            case preferredStatusBarStyle
            case progressTintColor
            case trackTintColor
            case pageIndicatorTintColor
            case currentPageIndicatorTintColor
            case image
            case tabbarImage
            case tabbarSelectedImage
            case activityIndicatorViewStyle
            case imageForStateNormal
            case imageForStateHighlighted
            case imageForStateSelected
            case imageForStateDisabled
            case backgroundImageForStateNormal
            case backgroundImageForStateHighlighted
            case backgroundImageForStateSelected
            case backgroundImageForStateDisabled
            case titleColorForStateNormal
            case titleColorForStateHighlighted
            case titleColorForStateSelected
            case titleColorForStateDisabled
            case borderWidth
            case borderColor
            case strokeColor
            case fillColor
            case custom(Key, Int)
            
            var value: String { return raw(from: self) }
            
            func raw(from key: Key) -> String {
                switch key {
                case let .custom(cKey, tag): return "\(raw(from: cKey))|\(tag)"
                default: return "\(key)"
                }
            }
        }
    }
}
