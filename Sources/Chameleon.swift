//
//  Chameleon.swift
//  Chameleon
//
//  Created by lincolnlaw on 2017/6/25.
//  Copyright © 2017年 lincolnlaw. All rights reserved.
//

import Foundation
// MARK: - Chameleon
public final class Chameleon {
    typealias Action = () -> Void
    private static let shared = Chameleon()
    public private(set) lazy var skin: Skin = Skin.none
    private init() {}
    private var _changingTimeInfo: (fadeIn: TimeInterval, showTime: TimeInterval, fadeOut: TimeInterval) = (0.3, 0.2, 0.3)
    private var _customView: UIView?

    private lazy var _mediaFolder: String = {
        let document = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first!
        let folder = "\(document)/Chameleon"
        FileManager.default.createDirectoryIfNotExist(at: folder)
        return folder
    }()

    private lazy var win: UIWindow = {
        let win = UIWindow()
        win.rootViewController = UIViewController()
        win.windowLevel = UIWindow.Level.statusBar + 1
        return win
    }()
}

// MARK: Public
extension Chameleon {
    public static var skin: Skin { return shared.skin }

    public static func mediaFolder(for style: Skin.Style) -> String {
        let path = "\(shared._mediaFolder)/\(style.name).chameleon"
        FileManager.default.createDirectoryIfNotExist(at: path)
        return path
    }
    
    public static func jsonFile(for style: Skin.Style) -> String {
        return "\(mediaFolder(for: style))/json"
    }

    /// change skin
    ///
    /// - Parameter skin: your custom skin
    public static func changeSkin(to skin: Skin) {

        var fromColor: UIColor?
        if let value = shared.skin.component(for: .backgroundColor), case let Chameleon.Component.backgroundColor(color) = value {
            fromColor = color
        }
        shared.skin = skin
        let win = shared.win
        var toColor: UIColor?
        if let value = shared.skin.component(for: .backgroundColor), case let Chameleon.Component.backgroundColor(color) = value {
            toColor = color
        }
        guard let from = fromColor, let to = toColor else {
            NotificationCenter.default.post(name: .ChameleonSkinChanged, object: nil)
            return
        }
        win.frame = UIScreen.main.bounds
        win.backgroundColor = from
        win.alpha = 0
        win.isHidden = false
        let fadeIn = shared._changingTimeInfo.fadeIn
        let showTime = shared._changingTimeInfo.showTime
        let fadeOt = shared._changingTimeInfo.fadeOut
        if let view = shared._customView { win.addSubview(view) }
        UIView.animate(withDuration: fadeIn, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0.9, options: .curveEaseOut, animations: {
            win.alpha = 1
            win.backgroundColor = to
        }, completion: { finish in
            if finish {
                NotificationCenter.default.post(name: .ChameleonSkinChanged, object: nil)
                DispatchQueue.main.asyncAfter(deadline: .now() + showTime, execute: {
                    UIView.animate(withDuration: fadeOt, animations: {
                        win.alpha = 0
                    }, completion: { finish in
                        if finish {
                            win.isHidden = true
                        }
                    })
                })
            }
        })
    }

    /// To day skin
    public static func toDay() { changeSkin(to: .day) }

    /// To night skin
    public static func toNight() { changeSkin(to: .night) }

    /// Custom skin changing loading state
    ///
    /// - Parameters:
    ///   - animationView: custom view to show animation
    ///   - fadeIn: timeinterval to fade in custom view
    ///   - showTime: timeinterval to show custom view
    ///   - fadeOut: timeinterval to fade out custom view
    public static func custom(animationView: UIView, fadeIn: TimeInterval = 0.3, showTime: TimeInterval = 0.2, fadeOut: TimeInterval = 0.3) {
        shared._changingTimeInfo = (fadeIn, showTime, fadeOut)
        shared._customView = animationView
    }
}
